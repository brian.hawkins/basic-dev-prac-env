import os
import unittest
import subprocess
from pathlib import Path

topic_defaults = {
    "C_Programming": 2,
    "Networking": 1,
    "Python": 2
}

questions_path = Path(".testbank/test-bank/")

def file_count(topic) -> int:
    return len(os.listdir(questions_path/topic))

def number_of(subdir):
    return len(os.listdir(Path("practice-exam")/Path(subdir)))

class TestPractice(unittest.TestCase):
    def test_default(self):
        subprocess.call("./createPracticeTest.py")
        self.assertEqual(number_of("C_Programming"),topic_defaults["C_Programming"])
        self.assertEqual(number_of("Networking"),topic_defaults["Networking"])
        self.assertEqual(number_of("Python"),topic_defaults["Python"])

    def test_specify(self):
        subprocess.call(["./createPracticeTest.py", "C", "5"])
        self.assertEqual(number_of("C_Programming"),5)
        self.assertEqual(number_of("Networking"),0)
        self.assertEqual(number_of("Python"),0)

    def test_specify_2(self):
        subprocess.call(["./createPracticeTest.py", "C", "5", "Python", "4"])
        self.assertEqual(number_of("C_Programming"),5)
        self.assertEqual(number_of("Networking"),0)
        self.assertEqual(number_of("Python"),4)

    def test_one_all(self):
        subprocess.call(["./createPracticeTest.py", "C", "all"])
        self.assertGreaterEqual(number_of("C_Programming"),file_count("C_Programming"))
        self.assertEqual(number_of("Networking"),0)
        self.assertEqual(number_of("Python"),0)

    def test_two_all(self):
        subprocess.call(["./createPracticeTest.py", "C", "all", "Networking", "all"])
        self.assertGreaterEqual(number_of("C_Programming"),file_count("C_Programming"))
        self.assertGreaterEqual(number_of("Networking"),file_count("Networking"))
        self.assertEqual(number_of("Python"),0)

    def test_all(self):
        subprocess.call(["./createPracticeTest.py", "all"])
        self.assertGreaterEqual(number_of("C_Programming"),file_count("C_Programming"))
        self.assertGreaterEqual(number_of("Networking"),file_count("Networking"))
        self.assertGreaterEqual(number_of("Python"),file_count("Python"))

if __name__ == '__main__':
    unittest.main()
