#!/usr/bin/python3

from ast import Store
import errno
import os
import sys
import subprocess
import random
import argparse

avoid_these = ['.DS_Store', 'VS_SLN_Template']

googletest_url = 'https://github.com/google/googletest.git'
basic_dev_url = 'https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file'
proj_path = os.path.abspath('./')
C_path = '.testbank/test-bank/C_Programming'
Python_path = '.testbank/test-bank/Python'
Networking_path = '.testbank/test-bank/Networking'
default_testbank_dir = ".testbank/test-bank"
default_user_dir = "practice-exam"

default_prefs = {
    "python" : 2,
    "c": 2,
    "networking": 1
}

zero_prefs = {
    "python" : 0,
    "c": 0,
    "networking": 0
}

def cleanTest():
    if os.path.exists(f'{default_user_dir}/C_Programming'):
        subprocess.call(f'rm -rf {default_user_dir}/C_Programming'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.call(f'mkdir {default_user_dir}/C_Programming'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if os.path.exists(f'{default_user_dir}/Python'):
        subprocess.call(f'rm -rf {default_user_dir}/Python'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.call(f'mkdir {default_user_dir}/Python'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if os.path.exists(f'{default_user_dir}/Networking'):
        subprocess.call(f'rm -rf {default_user_dir}/Networking'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.call(f'mkdir {default_user_dir}/Networking'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def getRandomQuestions(questions_pref):
    question_container = {
        'C': set(),
        'Python': set(),
        'Network': set()
    }

    add_questions(question_container['C'], questions_pref['c'], C_path)
    add_questions(question_container['Python'], questions_pref['python'], Python_path)
    add_questions(question_container['Network'], questions_pref['networking'], Networking_path)

    return question_container

def add_questions(container, number, path):
    try: 
        if int(number) > 0:
            while len(container) < int(number):
                new_question = random.choice(os.listdir(path))
                if new_question not in avoid_these and new_question not in container:
                    container.add(random.choice(os.listdir(path)))
    except ValueError:
        if number == "all":
            for new_question in os.listdir(path):
                if new_question not in avoid_these and new_question not in container:
                    container.add(new_question)


def copyQuestionToTest(questions):
    try:
        os.makedirs(default_user_dir)
    except OSError as e:
        if e.errno == errno.EEXIST:
            pass
    for question in questions['C']:
        subprocess.call(['cp', '-r', os.path.join(proj_path, C_path, question), f'{default_user_dir}/C_Programming/'])
        if not os.path.exists(f'{default_user_dir}/C_Programming/{question}/googletest'):
            subprocess.call(['cp', '-r', f'{proj_path}/googletest', f'{default_user_dir}/C_Programming/{question}/'])
    for question in questions['Python']:
        subprocess.call(['cp', '-r', os.path.join(proj_path, Python_path, question), f'{default_user_dir}/Python/'])
    for question in questions['Network']:
        subprocess.call(['cp', '-r', os.path.join(proj_path, Networking_path, question), f'{default_user_dir}/Networking/'])


def Convert(lst):
    res_dct = {lst[i].lower(): lst[i + 1] for i in range(0, len(lst), 2)}
    return res_dct

def get_topics(testbank_dir = default_testbank_dir):
    return ["c", "python", "networking"]


def print_topics():
    print("available topics: " + ", ".join(get_topics()))

def Convert(lst):
    res_dct = {lst[i].lower(): lst[i + 1] for i in range(0, len(lst), 2)}
    return res_dct

def get_topics(testbank_dir = default_testbank_dir):
    return ["c", "python", "networking"]


def print_topics():
    print("available topics: " + ", ".join(get_topics()))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("question_amounts", nargs='*', help="A repeating list in the format: Topic number e.g. Python 5 C all")
    parser.add_argument("--list_topics", help="get a list of available topics", action='store_true')
    args = parser.parse_args()
    if(args.list_topics):
        print_topics()
    if args.question_amounts:
        if len(args.question_amounts) % 2 == 0:
            question_prefs = zero_prefs.copy()
            question_prefs.update(Convert(args.question_amounts))
        else:
            if args.question_amounts[0] == "all":
                question_prefs = {
                    "python" : "all",
                    "c" : "all",
                    "networking" : "all"
                }
            else:
                print("usage: python3 createPracticeTest all")
                print("or")
                print("python3 createPracticeTest [topic1 amount [topic2 amount] ...]")
                return

    else:
        question_prefs = default_prefs
    
    cleanTest()
    
    if not os.path.exists('googletest'):
        subprocess.call(['git', 'clone', googletest_url])

    if not os.path.exists(".testbank"):
        subprocess.call(['git', 'clone', basic_dev_url, '.testbank'])
    
    copyQuestionToTest(getRandomQuestions(question_prefs))

    return 0

if __name__ == "__main__":
    main()
